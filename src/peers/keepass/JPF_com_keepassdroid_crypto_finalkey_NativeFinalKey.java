package keepass;

import gov.nasa.jpf.annotation.MJI;
import gov.nasa.jpf.vm.MJIEnv;
import gov.nasa.jpf.vm.NativePeer;

public class JPF_com_keepassdroid_crypto_finalkey_NativeFinalKey extends NativePeer {

  @MJI
  public static int nTransformMasterKey(MJIEnv env, int clsRef, int seed, int key, int rounds) {
    return key;

  }

  @MJI
  public static int nativeReflect(MJIEnv env, int clsRef, int key) {
    return key;

  }

}

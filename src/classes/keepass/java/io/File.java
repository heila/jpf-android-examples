package java.io;

import gov.nasa.jpf.annotation.FilterField;
import gov.nasa.jpf.vm.AndroidVerify;

public class File {
  public static final File TOP = new File("parent");
  public static final String separator = "" + "\\";

  private native void init0();

  @FilterField
  private String filename;

  public File(String filename) {
    this.filename = filename;
    init0();
  }

  public boolean exists() {
    return true;
  }

  public File getParentFile() {
    return TOP;
  }

  public boolean isDirectory() {
    return AndroidVerify.getBoolean("File.isDirectory");

  }

  public boolean mkdirs() {
    return true;
  }

  public boolean createNewFile() throws IOException {
    return true;
  }

  public boolean delete() {
    return true;
  }

  public String getAbsolutePath() {
    return "path";
  }

  public long length() {
    int i = AndroidVerify.getInt(0, 1, "File.getLength");
    // if (i == 0) {
    // return 0;
    // } else
    if (i == 1) {
      return 32;
    } else {
      return 64;
    }

  }

  public boolean renameTo(File f) {
    return true;
  }

  public File[] listFiles() {
    // TODO Auto-generated method stub
    return null;
  }

  public String getName() {
    // TODO Auto-generated method stub
    return null;
  }

  public int getAbsoluteFile() {
    // TODO Auto-generated method stub
    return 0;
  }

  public boolean isFile() {
    // TODO Auto-generated method stub
    return false;
  }
}

/*
 * Copyright (C) 2014, United States Government, as represented by the
 * Administrator of the National Aeronautics and Space Administration.
 * All rights reserved.
 *
 * The Java Pathfinder core (jpf-core) platform is licensed under the
 * Apache License, Version 2.0 (the "License"); you may not use this file except
 * in compliance with the License. You may obtain a copy of the License at
 * 
 *        http://www.apache.org/licenses/LICENSE-2.0. 
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and 
 * limitations under the License.
 */
package java.io;

import java.nio.channels.FileChannel;

/**
 * MJI model class for java.io.RandomAccessFile This class cannot yet handle File I/O correctly Some work
 * about the use of RandomAccessFile can be found here : https://bitbucket.org/pfjeau/jpf_for_nanohttpd/src/8f
 * 880ee27410026c69cf37f1904b159965d1576e/?at=raf-progress Another way to implement all the missing features
 * is to fix the jpf-bfs project in order to handle file I/O
 *
 * @author Owen O'Malley
 */
@SuppressWarnings("unused")
public class RandomAccessFile {
  public RandomAccessFile(File name, String permissions) throws FileNotFoundException {

  }

  public RandomAccessFile(String name, String permissions) throws FileNotFoundException {

  }

  public void seek(long posn) throws IOException {

  }

  public long length() throws IOException {
    return 64;

  }

  public void setDataMap() {

  }

  public void writeByte(int data) throws IOException {

  }

  public void write(byte[] data, int start, int len) throws IOException {

  }

  public void setLength(long len) throws IOException {

  }

  public int read(byte[] data, int start, int len) throws IOException {
    return len;
  }

  public byte readByte() throws IOException {
    return 1;
  }

  public void close() throws IOException {

  }

  public FileChannel getChannel() {
    return null;// TODO
  }

  public FileDescriptor getFD() {
    return null;// TODO
  }

}

/*
 * Copyright (C) 2014, United States Government, as represented by the
 * Administrator of the National Aeronautics and Space Administration.
 * All rights reserved.
 *
 * The Java Pathfinder core (jpf-core) platform is licensed under the
 * Apache License, Version 2.0 (the "License"); you may not use this file except
 * in compliance with the License. You may obtain a copy of the License at
 * 
 *        http://www.apache.org/licenses/LICENSE-2.0. 
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and 
 * limitations under the License.
 */
package java.io;

import java.nio.channels.FileChannel;

public class FileOutputStream extends OutputStream {

  public FileOutputStream(String fname) throws FileNotFoundException {

  }

  public FileOutputStream(File file) throws FileNotFoundException {
  }

  public FileOutputStream(FileDescriptor fd) {
  }

  public FileChannel getChannel() {
    return null;
  }

  public FileDescriptor getFD() {
    return new FileDescriptor();
  }

  // --- our native peer methods

  boolean open(String fname) {
    // this sets the FileDescriptor from the peer side
    return true;
  }

  @Override
  public void write(int b) throws IOException {
  }

  @Override
  public void write(byte[] buf, int off, int len) throws IOException {
  }

  @Override
  public void close() throws IOException {
  }

  @Override
  public void flush() throws IOException {
  }
}

package java.security;

public class AlgorithmParameters {
  public static java.security.AlgorithmParameters TOP = new java.security.AlgorithmParameters();


  public AlgorithmParameters(){
  }

  public final java.security.spec.AlgorithmParameterSpec getParameterSpec(java.lang.Class param0) throws java.security.spec.InvalidParameterSpecException {
    return javax.crypto.spec.IvParameterSpec.TOP;
  }
}
package javax.crypto;
import gov.nasa.jpf.vm.Abstraction;
import gov.nasa.jpf.vm.Verify;

public class Cipher {
  public static javax.crypto.Cipher TOP = new javax.crypto.Cipher();


  public Cipher(){
  }

  public static final javax.crypto.Cipher getInstance(java.lang.String param0, java.security.Provider param1) throws java.security.NoSuchAlgorithmException ,javax.crypto.NoSuchPaddingException {
    return javax.crypto.Cipher.TOP;
  }

  public static final javax.crypto.Cipher getInstance(java.lang.String param0) throws java.security.NoSuchAlgorithmException ,javax.crypto.NoSuchPaddingException {
    return javax.crypto.Cipher.TOP;
  }

  public final void init(int param0, java.security.Key param1, java.security.spec.AlgorithmParameterSpec param2) throws java.security.InvalidKeyException ,java.security.InvalidAlgorithmParameterException {
  }

  public final int doFinal(byte[] param0, int param1, int param2, byte[] param3, int param4) throws javax.crypto.ShortBufferException ,javax.crypto.IllegalBlockSizeException ,javax.crypto.BadPaddingException {
    return Abstraction.TOP_INT;
  }

  public final byte[] doFinal() throws javax.crypto.IllegalBlockSizeException ,javax.crypto.BadPaddingException {
    return ((byte[])Verify.randomObject("byte[]"));
  }

  public final byte[] update(byte[] param0, int param1, int param2){
    return ((byte[])Verify.randomObject("byte[]"));
  }

  public final void init(int param0, java.security.Key param1) throws java.security.InvalidKeyException {
  }

  public final int update(byte[] param0, int param1, int param2, byte[] param3, int param4) throws javax.crypto.ShortBufferException {
    return Abstraction.TOP_INT;
  }
}
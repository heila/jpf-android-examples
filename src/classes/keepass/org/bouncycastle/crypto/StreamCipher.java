package org.bouncycastle.crypto;

public interface StreamCipher {


  public abstract void init(boolean param0, org.bouncycastle.crypto.CipherParameters param1) throws java.lang.IllegalArgumentException ;

  public abstract void processBytes(byte[] param0, int param1, int param2, byte[] param3, int param4) throws org.bouncycastle.crypto.DataLengthException ;
}
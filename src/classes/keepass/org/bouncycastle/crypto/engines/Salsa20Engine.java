package org.bouncycastle.crypto.engines;

public class Salsa20Engine implements org.bouncycastle.crypto.StreamCipher {
  public static org.bouncycastle.crypto.engines.Salsa20Engine TOP = new org.bouncycastle.crypto.engines.Salsa20Engine();


  public Salsa20Engine(){
  }

  public void init(boolean param0, org.bouncycastle.crypto.CipherParameters param1) throws java.lang.IllegalArgumentException {
  }

  public void processBytes(byte[] param0, int param1, int param2, byte[] param3, int param4) throws org.bouncycastle.crypto.DataLengthException {
  }
}
package android.text.util;
import gov.nasa.jpf.vm.Abstraction;

public class Rfc822Tokenizer {
  public static android.text.util.Rfc822Tokenizer TOP = new android.text.util.Rfc822Tokenizer();


  public Rfc822Tokenizer(){
  }

  public static android.text.util.Rfc822Token[] tokenize(java.lang.CharSequence param0){
    return ((android.text.util.Rfc822Token[])Abstraction.randomObject("android.text.util.Rfc822Token[]"));
  }
}
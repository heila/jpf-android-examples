package android.text;
import gov.nasa.jpf.vm.Abstraction;

public class TextUtils {
  public static android.text.TextUtils TOP = new android.text.TextUtils();


  public TextUtils(){
  }

  public static boolean isEmpty(java.lang.CharSequence param0){
    return Abstraction.TOP_BOOL;
  }

  public static java.lang.String htmlEncode(java.lang.String param0){
    return Abstraction.TOP_STRING;
  }
}
package android.text.method;
import gov.nasa.jpf.vm.Abstraction;

public class DigitsKeyListener {
  public static android.text.method.DigitsKeyListener TOP = new android.text.method.DigitsKeyListener();


  public DigitsKeyListener(){
  }

  public static android.text.method.DigitsKeyListener getInstance(java.lang.String param0){
    return ((android.text.method.DigitsKeyListener)Abstraction.randomObject("android.text.method.DigitsKeyListener"));
  }
}
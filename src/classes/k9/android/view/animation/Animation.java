package android.view.animation;


public class Animation {
  public static android.view.animation.Animation TOP = new android.view.animation.Animation();

  public static class AnimationListener {

  }

  public Animation() {
  }

  public void setAnimationListener(android.view.animation.Animation.AnimationListener param0) {
  }

  public void setDuration(long param0) {
  }

  public void setFillBefore(boolean param0) {
  }

  public void setInterpolator(android.view.animation.Interpolator param0) {
  }
}
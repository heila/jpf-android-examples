package android.webkit;
import gov.nasa.jpf.vm.Abstraction;

public class MimeTypeMap {
  public static android.webkit.MimeTypeMap TOP = new android.webkit.MimeTypeMap();


  public MimeTypeMap(){
  }

  public static android.webkit.MimeTypeMap getSingleton(){
    return ((android.webkit.MimeTypeMap)Abstraction.randomObject("android.webkit.MimeTypeMap"));
  }

  public java.lang.String getMimeTypeFromExtension(java.lang.String param0){
    return Abstraction.TOP_STRING;
  }
}
package com.beetstra.jutf7;
import gov.nasa.jpf.vm.Abstraction;

public class CharsetProvider {
  public static com.beetstra.jutf7.CharsetProvider TOP = new com.beetstra.jutf7.CharsetProvider();


  public CharsetProvider(){
  }

   java.nio.charset.Charset charsetForName(java.lang.String param0){
    return ((java.nio.charset.Charset)Abstraction.randomObject("java.nio.charset.Charset"));
  }
}
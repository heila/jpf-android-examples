package org.apache.commons.io;
import gov.nasa.jpf.vm.Abstraction;

public class IOUtils {
  public static org.apache.commons.io.IOUtils TOP = new org.apache.commons.io.IOUtils();


  public IOUtils(){
  }

  public static int copy(java.io.InputStream param0, java.io.OutputStream param1) throws java.io.IOException {
    return Abstraction.TOP_INT;
  }

  public static java.lang.String toString(java.io.InputStream param0, java.lang.String param1) throws java.io.IOException {
    return Abstraction.TOP_STRING;
  }
}
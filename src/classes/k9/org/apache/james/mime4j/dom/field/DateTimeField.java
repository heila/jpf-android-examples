package org.apache.james.mime4j.dom.field;

public interface DateTimeField extends org.apache.james.mime4j.dom.field.ParsedField {


  public abstract java.util.Date getDate();
}
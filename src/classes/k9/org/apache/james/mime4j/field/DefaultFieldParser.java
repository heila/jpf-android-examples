package org.apache.james.mime4j.field;
import gov.nasa.jpf.vm.Abstraction;

public class DefaultFieldParser {
  public static org.apache.james.mime4j.field.DefaultFieldParser TOP = new org.apache.james.mime4j.field.DefaultFieldParser();


  public DefaultFieldParser(){
  }

  public static org.apache.james.mime4j.dom.field.ParsedField parse(java.lang.String param0) throws org.apache.james.mime4j.MimeException {
    return ((org.apache.james.mime4j.dom.field.ParsedField)Abstraction.randomObject("org.apache.james.mime4j.dom.field.ParsedField"));
  }
}
package org.apache.http.conn;
import gov.nasa.jpf.vm.Abstraction;

public class ClientConnectionManagerImpl implements org.apache.http.conn.ClientConnectionManager {
  public static org.apache.http.conn.ClientConnectionManagerImpl TOP = new org.apache.http.conn.ClientConnectionManagerImpl();


  public org.apache.http.conn.scheme.SchemeRegistry getSchemeRegistry(){
    return ((org.apache.http.conn.scheme.SchemeRegistry)Abstraction.randomObject("org.apache.http.conn.scheme.SchemeRegistry"));
  }
}
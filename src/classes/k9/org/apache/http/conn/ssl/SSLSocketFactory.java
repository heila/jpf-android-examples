package org.apache.http.conn.ssl;
import gov.nasa.jpf.vm.Abstraction;

public class SSLSocketFactory implements org.apache.http.conn.scheme.LayeredSocketFactory {
  public static final org.apache.http.conn.ssl.X509HostnameVerifier ALLOW_ALL_HOSTNAME_VERIFIER = ((org.apache.http.conn.ssl.X509HostnameVerifier)Abstraction.randomObject("org.apache.http.conn.ssl.X509HostnameVerifier"));
  public static org.apache.http.conn.ssl.SSLSocketFactory TOP = new org.apache.http.conn.ssl.SSLSocketFactory();


  public SSLSocketFactory(){
  }

  public static org.apache.http.conn.ssl.SSLSocketFactory getSocketFactory(){
    return ((org.apache.http.conn.ssl.SSLSocketFactory)Abstraction.randomObject("org.apache.http.conn.ssl.SSLSocketFactory"));
  }

  public void setHostnameVerifier(org.apache.http.conn.ssl.X509HostnameVerifier param0){
  }

  public java.net.Socket connectSocket(java.net.Socket param0, java.lang.String param1, int param2, java.net.InetAddress param3, int param4, org.apache.http.params.HttpParams param5) throws java.io.IOException {
    return ((java.net.Socket)Abstraction.randomObject("java.net.Socket"));
  }

  public boolean isSecure(java.net.Socket param0) throws java.lang.IllegalArgumentException {
    return Abstraction.TOP_BOOL;
  }
}
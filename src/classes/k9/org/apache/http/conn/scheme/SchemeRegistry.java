package org.apache.http.conn.scheme;
import gov.nasa.jpf.vm.Abstraction;

public final class SchemeRegistry {
  public static org.apache.http.conn.scheme.SchemeRegistry TOP = new org.apache.http.conn.scheme.SchemeRegistry();


  public SchemeRegistry(){
  }

  public final synchronized org.apache.http.conn.scheme.Scheme register(org.apache.http.conn.scheme.Scheme param0){
    return ((org.apache.http.conn.scheme.Scheme)Abstraction.randomObject("org.apache.http.conn.scheme.Scheme"));
  }
}
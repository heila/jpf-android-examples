package org.apache.http.impl.client;
import gov.nasa.jpf.vm.Abstraction;

public class BasicCookieStore implements org.apache.http.client.CookieStore {
  public static org.apache.http.impl.client.BasicCookieStore TOP = new org.apache.http.impl.client.BasicCookieStore();


  public BasicCookieStore(){
  }

  public void clear(){
  }

  public java.util.List getCookies(){
    return ((java.util.List)Abstraction.randomObject("java.util.List"));
  }
}
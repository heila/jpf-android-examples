package oauth.signpost.commonshttp;

import gov.nasa.jpf.vm.Abstraction;

public class CommonsHttpOAuthConsumer {
  public static oauth.signpost.commonshttp.CommonsHttpOAuthConsumer TOP = new oauth.signpost.commonshttp.CommonsHttpOAuthConsumer();

  public CommonsHttpOAuthConsumer() {
  }

  public CommonsHttpOAuthConsumer(java.lang.String param0, java.lang.String param1) {
  }

  public String getToken() {
    return Abstraction.TOP_STRING;
  }

  public String getTokenSecret() {
    return Abstraction.TOP_STRING;

  }

}
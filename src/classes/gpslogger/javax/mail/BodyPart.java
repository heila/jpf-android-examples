package javax.mail;
import gov.nasa.jpf.vm.Verify;
import gov.nasa.jpf.vm.Abstraction;

public class BodyPart implements javax.mail.Part {
  public static javax.mail.BodyPart TOP = new javax.mail.BodyPart();


  public BodyPart(){
  }

  public void setText(java.lang.String param0) throws javax.mail.MessagingException {
  }

  public void setDataHandler(javax.activation.DataHandler param0) throws javax.mail.MessagingException {
  }

  public void setFileName(java.lang.String param0) throws javax.mail.MessagingException {
  }
}
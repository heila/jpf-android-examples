package javax.mail;
import gov.nasa.jpf.vm.Verify;
import gov.nasa.jpf.vm.Abstraction;

public final class Session {
  public static javax.mail.Session TOP = new javax.mail.Session();


  public Session(){
  }

  public static javax.mail.Session getInstance(java.util.Properties param0, javax.mail.Authenticator param1){
    return ((javax.mail.Session)Abstraction.randomObject("javax.mail.Session"));
  }
}
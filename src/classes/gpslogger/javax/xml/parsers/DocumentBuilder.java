package javax.xml.parsers;
import gov.nasa.jpf.vm.Verify;
import gov.nasa.jpf.vm.Abstraction;

public class DocumentBuilder {
  public static javax.xml.parsers.DocumentBuilder TOP = new javax.xml.parsers.DocumentBuilder();


  public DocumentBuilder(){
  }

  public org.w3c.dom.Document parse(java.io.File param0) throws org.xml.sax.SAXException ,java.io.IOException {
    return ((org.w3c.dom.Document)Abstraction.randomObject("org.w3c.dom.Document"));
  }
}
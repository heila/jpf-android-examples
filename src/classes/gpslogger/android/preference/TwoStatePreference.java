package android.preference;
import gov.nasa.jpf.vm.AndroidVerify;

public class TwoStatePreference extends android.preference.Preference {
  public static android.preference.TwoStatePreference TOP = new android.preference.TwoStatePreference();


  public TwoStatePreference(){
  }

  public boolean isChecked(){
    return AndroidVerify.getBoolean("TwoStatePreference.isChecked()");
  }

  public void setChecked(boolean param0){
  }
}
package android.app;

import gov.nasa.jpf.annotation.FilterField;
import gov.nasa.jpf.annotation.NeverBreak;
import gov.nasa.jpf.vm.AndroidVerify;

import java.io.File;
import java.util.HashMap;
import java.util.Map;
import java.util.Set;

public class SharedPreferencesImpl implements android.content.SharedPreferences {
  public final HashMap<OnSharedPreferenceChangeListener, Object> mListeners = new HashMap<OnSharedPreferenceChangeListener, Object>();

  @FilterField
  @NeverBreak
  private final Object mContent = new Object();

  public SharedPreferencesImpl() {
  }

  public SharedPreferencesImpl(File f, int i) {
  }

  public java.lang.String getString(java.lang.String param0, java.lang.String param1) {
    String var = param1;
    if (param0.equals("locale_override")) {
      var = "";
    }
    if (param0.equals("distance_before_logging")) {
      var = "1";
    }
    if (param0.equals("time_before_logging")) {
      var = "1";
    }
    if (param0.equals("new_file_creation")) {
      var = "onceaday";
    }
    if (param0.equals("autoemail_frequency")) {
      var = "0.08";
    }
    if (param0.equals("smtp_server")) {
      var = "smtp.mail.yahoo.com";
    }
    if (param0.equals("smtp_port")) {
      var = "465";
    }
    if (param0.equals("smtp_username")) {
      var = "testemail";
    }
    if (param0.equals("smtp_password")) {
      var = "1234567";
    }
    if (param0.equals("autoemail_target")) {
      var = "testemail@yahoo.com";
    }
    if (param0.equals("osm_accesstoken")) {
      var = "";
    }
    return var;
  }

  @NeverBreak
  @FilterField
  EditorImpl editor = new EditorImpl();

  public android.content.SharedPreferences.Editor edit() {
    return editor;
  }

  public int getInt(java.lang.String param0, int param1) {
    return param1;
  }

  public boolean getBoolean(java.lang.String param0, boolean param1) {
    return AndroidVerify.getBoolean("android.content.SharedPreferences(" + param0 + ")");
  }

  public long getLong(java.lang.String param0, long param1) {
    return param1;
  }

  @Override
  public void registerOnSharedPreferenceChangeListener(OnSharedPreferenceChangeListener listener) {
    synchronized (this) {
      mListeners.put(listener, mContent);
    }
  }

  @Override
  public void unregisterOnSharedPreferenceChangeListener(OnSharedPreferenceChangeListener listener) {
    synchronized (this) {
      mListeners.remove(listener);
    }
  }

  public final class EditorImpl implements Editor {

    @Override
    public Editor putString(String key, String value) {
      return this;
    }

    @Override
    public Editor putStringSet(String key, Set<String> values) {
      return this;
    }

    @Override
    public Editor putInt(String key, int value) {
      return this;
    }

    @Override
    public Editor putLong(String key, long value) {
      return this;
    }

    @Override
    public Editor putFloat(String key, float value) {
      return this;
    }

    @Override
    public Editor putBoolean(String key, boolean value) {
      return this;
    }

    @Override
    public Editor remove(String key) {
      return this;
    }

    @Override
    public Editor clear() {
      return this;
    }

    @Override
    public boolean commit() {
      return true;
    }

    @Override
    public void apply() {
    }
  }

  @Override
  public Map<String, ?> getAll() {
    return null;
  }

  @Override
  public Set<String> getStringSet(String key, Set<String> defValues) {
    return defValues;
  }

  @Override
  public float getFloat(String key, float defValue) {
    return defValue;
  }

  @Override
  public boolean contains(String key) {
    return true;
  }

  @Override
  public Map<OnSharedPreferenceChangeListener, Object> getListeners() {
    return mListeners;
  }
}
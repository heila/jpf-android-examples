package android.widget;

import android.content.Context;
import android.util.AttributeSet;

public class ToggleButton extends android.widget.CompoundButton {

  public ToggleButton(Context c) {
    super(c);
  }

  public ToggleButton(Context c, AttributeSet att) {
    super(c);
  }

}
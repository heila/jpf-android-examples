package android.content;

import gov.nasa.jpf.vm.Abstraction;
import android.os.Parcel;

public class SyncAdapterType implements android.os.Parcelable {
  public java.lang.String accountType = Abstraction.TOP_STRING;
  public static android.content.SyncAdapterType TOP = new android.content.SyncAdapterType();


  public SyncAdapterType(){
  }

  public boolean supportsUploading(){
    return true;
  }

  @Override
  public int describeContents() {
    return 0;
  }

  @Override
  public void writeToParcel(Parcel dest, int flags) {
  }
}
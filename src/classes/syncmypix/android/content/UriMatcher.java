package android.content;

import gov.nasa.jpf.vm.Abstraction;

public class UriMatcher {
  public static android.content.UriMatcher TOP = new android.content.UriMatcher();

  public UriMatcher() {
  }

  public UriMatcher(int param0) {
  }

  public void addURI(java.lang.String param0, java.lang.String param1, int param2) {
  }

  public int match(android.net.Uri param0) {
    return Abstraction.TOP_INT;
  }
}
package android.graphics;

import gov.nasa.jpf.vm.Abstraction;
import android.os.Parcel;

public class RectF implements android.os.Parcelable {
  public float left;
  public float top;
  public float right;
  public float bottom;
  public static final android.graphics.RectF TOP = new android.graphics.RectF();


  public RectF(){
  }

  public RectF(android.graphics.Rect param0){
  }

  public final float width(){
    return Abstraction.TOP_FLOAT;
  }

  public final float height(){
    return Abstraction.TOP_FLOAT;
  }

  public void offset(float param0, float param1){
  }

  public RectF(android.graphics.RectF param0){
  }

  public void inset(float param0, float param1){
  }

  public void set(android.graphics.RectF param0){
  }

  public RectF(float param0, float param1, float param2, float param3){
  }

  @Override
  public int describeContents() {
    return 0;
  }

  @Override
  public void writeToParcel(Parcel dest, int flags) {
  }
}
package android.graphics;

import gov.nasa.jpf.vm.Abstraction;


public class Matrix {
  public static final android.graphics.Matrix TOP = new android.graphics.Matrix();

  public static Matrix IDENTITY_MATRIX = new Matrix();

  public Matrix() {
  }

  public boolean postScale(float param0, float param1) {
    return Abstraction.TOP_BOOL;
  }

  public boolean mapRect(android.graphics.RectF param0) {
    return Abstraction.TOP_BOOL;
  }

  public Matrix(android.graphics.Matrix param0) {
  }

  public void mapPoints(float[] param0) {
  }

  public void reset() {
  }

  public void set(android.graphics.Matrix param0) {
  }

  public void getValues(float[] param0) {
  }

  public void setScale(float param0, float param1) {
  }

  public boolean postTranslate(float param0, float param1) {
    return Abstraction.TOP_BOOL;
  }

  public boolean postConcat(android.graphics.Matrix param0) {
    return Abstraction.TOP_BOOL;
  }

  public boolean postScale(float param0, float param1, float param2, float param3) {
    return Abstraction.TOP_BOOL;
  }

  public void setScale(float param0, float param1, float param2, float param3) {
  }
}
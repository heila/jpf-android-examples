package android.database.sqlite;

public class SQLiteQueryBuilder {
  public static final android.database.sqlite.SQLiteQueryBuilder TOP = new android.database.sqlite.SQLiteQueryBuilder();

  public SQLiteQueryBuilder() {
  }

  public void setTables(java.lang.String param0) {
  }

  public void setProjectionMap(java.util.Map param0) {
  }

  public void appendWhere(java.lang.CharSequence param0) {
  }

  public android.database.Cursor query(android.database.sqlite.SQLiteDatabase param0,
                                       java.lang.String[] param1, java.lang.String param2,
                                       java.lang.String[] param3, java.lang.String param4,
                                       java.lang.String param5, java.lang.String param6) {
    return new android.database.CursorImpl();
  }
}
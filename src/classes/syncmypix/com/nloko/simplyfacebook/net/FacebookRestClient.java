package com.nloko.simplyfacebook.net;

import gov.nasa.jpf.vm.AndroidVerify;

import java.io.IOException;
import java.util.Map;

import org.apache.http.client.ClientProtocolException;

public class FacebookRestClient {

  public FacebookRestClient(String apiKey, String uid, String session, String secret) {

  }

  public Response getData(String method, Map<String, String> params) throws ClientProtocolException,
      IOException {
    if (AndroidVerify.getBoolean("FacebookRestClient.getData")) {
      return FacebookJSONResponse.TOP;
    } else
      throw new IOException("FacebookRestClient.getData failed");

  }

  public Response getData(String string) {
    return FacebookJSONResponse.TOP;

  }

}

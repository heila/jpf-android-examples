package org.apache.http.client;

public interface HttpClient {


  public abstract org.apache.http.HttpResponse execute(org.apache.http.client.methods.HttpUriRequest param0) throws java.io.IOException ,org.apache.http.client.ClientProtocolException ;

  public abstract org.apache.http.conn.ClientConnectionManager getConnectionManager();
}
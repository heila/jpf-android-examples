package models;

import gov.nasa.jpf.vm.Abstraction;
import gov.nasa.jpf.vm.AndroidVerify;

import java.io.ByteArrayInputStream;
import java.io.InputStream;

import android.content.ContentProvider;
import android.content.ContentResolver;
import android.content.ContentValues;
import android.database.Cursor;
import android.database.DefaultCursor;
import android.net.Uri;
import android.os.CancellationSignal;

public class ContactsContentProvider extends ContentProvider {

  public static class Contacts {
    /**
     * Opens an InputStream for the contacts's thumbnail photo and returns the photo as a byte stream.
     * 
     * @param cr
     *          The content resolver to use for querying
     * @param contactUri
     *          the contact whose photo should be used. This can be used with either a {@link #CONTENT_URI} or
     *          a {@link #CONTENT_LOOKUP_URI} URI.
     * @return an InputStream of the photo, or null if no photo is present
     * @see #openContactPhotoInputStream(ContentResolver, Uri, boolean), if instead of the thumbnail the
     *      high-res picture is preferred
     */
    static final InputStream i = new ByteArrayInputStream(new byte[] { 0, 1, 2 });;

    public static InputStream openContactPhotoInputStream(ContentResolver cr, Uri contactUri) {
      return i;
    }
  }

  @Override
  public boolean onCreate() {
    return true;
  }

  @Override
  public Cursor query(Uri param0, String[] param1, String param2, String[] param3, String param4,
                      CancellationSignal param5) {
    return new ContactsCursor();
  }

  @Override
  public Cursor query(Uri uri, String[] projection, String selection, String[] selectionArgs, String sortOrder) {
    return new ContactsCursor();
  }

  @Override
  public String getType(Uri uri) {
    return null;
  }

  @Override
  public Uri insert(Uri uri, ContentValues values) {
    System.out.println("###ContactsContentProvider");
    return Uri.withAppendedPath(uri, "1");

  }

  @Override
  public int delete(Uri uri, String selection, String[] selectionArgs) {
    return 0;
  }

  @Override
  public int update(Uri uri, ContentValues values, String selection, String[] selectionArgs) {
    return values.size();

  }

  public static class ContactsCursor extends DefaultCursor {
    @Override
    public int getColumnIndex(String columnName) {
      System.out.println("getColumnIndex " + columnName);

      if (columnName.equals("_id"))
        return 0;
      else if (columnName.equals("friend_id"))
        return 1;
      else if (columnName.equals("contact_id"))
        return 2;
      else if (columnName.equals("photo_hash"))
        return 3;
      else if (columnName.equals("lookup_key"))
        return 101;
      else if (columnName.equals("updated"))
        return 102;
      else if (columnName.equals("skipped"))
        return 103;
      else if (columnName.equals("not_found"))
        return 104;
      else if (columnName.equals("pic_url"))
        return 105;
      else
        return 100;
    }

    @Override
    public byte[] getBlob(int columnIndex) {
      return new byte[] { 0, 0, 0, 0 };
    }

    @Override
    public String getString(int columnIndex) {

      // TODO Auto-generated method stub
      // if (columnIndex == 2) {
      // return "1L";
      // } else
      if (columnIndex < 100) {
        System.out.println("getString " + columnIndex + "->1");
        return "1";
      } else {
        System.out.println("getString " + columnIndex + "->TOP");

        return Abstraction.TOP_STRING;
      }
    }

    @Override
    public int getInt(int columnIndex) {
      if (columnIndex == 0) {
        return AndroidVerify.getInt(-1, 0, "ContactsCursor.getInt(" + columnIndex + ")");
      }
      return 2;
    }

  }
}

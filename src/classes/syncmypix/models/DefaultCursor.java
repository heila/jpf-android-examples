package models;

import gov.nasa.jpf.annotation.FilterField;
import gov.nasa.jpf.vm.Abstraction;
import android.content.ContentResolver;
import android.database.CharArrayBuffer;
import android.database.ContentObserver;
import android.database.DataSetObserver;
import android.net.Uri;
import android.os.Bundle;

public class DefaultCursor implements android.database.Cursor {

  @FilterField
  private int max = 1;
  private int count = -1;

  @FilterField
  Uri uri = null;

  public DefaultCursor() {
    max = 2;// (int) AndroidVerify.getValues(new Object[] { 0, max }, "DefaultCursor.getCount()");
  }

  public DefaultCursor(int num) {
    max = num;// (int) AndroidVerify.getValues(new Object[] { 0, max }, "DefaultCursor.getCount()");
  }

  public Uri getUri() {
    return uri;
  }

  public void setUri(Uri uri) {
    this.uri = uri;
  }

  @Override
  public boolean moveToFirst() {
    count = 0;
    return true;
  }

  @Override
  public int getColumnIndex(java.lang.String param0) {
    return 0;
  }

  @Override
  public java.lang.String getString(int param0) {
    return Abstraction.TOP_STRING;
  }

  @Override
  public long getLong(int param0) {
    return Abstraction.TOP_LONG;
  }

  @Override
  public boolean moveToNext() {
    if (count <= max) {
      count++;
      return true;
    }
    return false;
  }

  @Override
  public int getCount() {
    return max;
  }

  @Override
  public int getPosition() {
    return count;
  }

  @Override
  public boolean move(int offset) {
    if (count + offset <= max + 1 || count + offset >= -1) {
      count = count + offset;
      return true;
    }
    return false;
  }

  @Override
  public boolean moveToPosition(int position) {
    if ((-1 <= position) && (position <= max + 1)) {
      count = position;
      return true;
    }
    return false;
  }

  @Override
  public boolean moveToLast() {
    count = max;
    return true;
  }

  @Override
  public boolean moveToPrevious() {
    if (count > -1) {
      count--;
      return true;
    }
    return false;
  }

  @Override
  public boolean isFirst() {
    return count == 0;
  }

  @Override
  public boolean isLast() {
    return count == max;
  }

  @Override
  public boolean isBeforeFirst() {
    return count == -1;
  }

  @Override
  public boolean isAfterLast() {
    return count > max;
  }

  @Override
  public int getColumnIndexOrThrow(String columnName) throws IllegalArgumentException {
    return 0;
  }

  @Override
  public String getColumnName(int columnIndex) {
    return "ColumnName";
  }

  @Override
  public String[] getColumnNames() {
    return null;
  }

  @Override
  public int getColumnCount() {
    return 1;
  }

  @Override
  public byte[] getBlob(int columnIndex) {
    return new byte[] { 0 };
  }

  @Override
  public void copyStringToBuffer(int columnIndex, CharArrayBuffer buffer) {
  }

  @Override
  public short getShort(int columnIndex) {
    return 0;
  }

  @Override
  public int getInt(int columnIndex) {
    return 0;
  }

  @Override
  public float getFloat(int columnIndex) {
    return 0;
  }

  @Override
  public double getDouble(int columnIndex) {
    return 0;
  }

  @Override
  public int getType(int columnIndex) {
    return 0;
  }

  @Override
  public boolean isNull(int columnIndex) {
    return false;
  }

  @Override
  public void deactivate() {
  }

  @Override
  public boolean requery() {
    return true;
  }

  boolean isClosed = false;

  @Override
  public void close() {
    isClosed = true;

  }

  @Override
  public boolean isClosed() {
    return isClosed;
  }

  @Override
  public void registerContentObserver(ContentObserver observer) {
  }

  @Override
  public void unregisterContentObserver(ContentObserver observer) {

  }

  @Override
  public void registerDataSetObserver(DataSetObserver observer) {
    obs = observer;
  }

  @Override
  public void unregisterDataSetObserver(DataSetObserver observer) {
    obs = null;
  }

  @FilterField
  DataSetObserver obs;

  @Override
  public void setNotificationUri(ContentResolver cr, Uri uri) {

  }

  @Override
  public Uri getNotificationUri() {
    return null;
  }

  @Override
  public boolean getWantsAllOnMoveCalls() {
    return true;
  }

  @Override
  public Bundle getExtras() {
    return null;
  }

  @Override
  public Bundle respond(Bundle extras) {
    return null;
  }
}
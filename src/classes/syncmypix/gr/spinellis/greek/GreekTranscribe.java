/* The following code was generated by JFlex 1.4.3 on 25/02/12 21:15 */

/**
 * JFlex specification for transcribing Greek characters into latin
 * according to ISO 843:1997.
 *
 *    GreekTranscribe.java is part of SyncMyPix
 *
 *    Author: Diomidis Spinellis <dds@aueb.gr>
 *
 *    Copyright (c) 2012 Diomidis Spinellis
 *
 *    SyncMyPix is free software: you can redistribute it and/or modify
 *    it under the terms of the GNU General Public License as published by
 *    the Free Software Foundation, either version 3 of the License, or
 *    (at your option) any later version.
 *
 *    SyncMyPix is distributed in the hope that it will be useful,
 *    but WITHOUT ANY WARRANTY; without even the implied warranty of
 *    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *    GNU General Public License for more details.
 *
 *    You should have received a copy of the GNU General Public License
 *    along with SyncMyPix.  If not, see <http://www.gnu.org/licenses/>.
 *
 * Convert this file into Java using a sequence line the following
 * grconv -S UTF-8 -T Java greek-transcribe.lex >foo.lex
 * jflex foo.lex
 *
 * See also
 * http://www.spinellis.gr/sw/greek/grconv/
 * http://jflex.de/faq.html
 * http://transliteration.eki.ee/pdf/Greek.pdf
 */

package gr.spinellis.greek;

/**
 * Transcribe Greek input per ISO 843:1997 part 2
 */

public class GreekTranscribe {

  /**
   * Convenience method that returns the passed string transcribed.
   */
  public static String string(String in) {
    System.out.println("TESTING$$$$$$$$$$$$$$$$$$$$$$$$$$$$");
    return in;

  }

  /**
   * Creates a new scanner There is also a java.io.InputStream version of this
   * constructor.
   *
   * @param in
   *          the java.io.Reader to read input from.
   */
  public GreekTranscribe(java.io.Reader in) {
  }

  /**
   * Creates a new scanner. There is also java.io.Reader version of this
   * constructor.
   *
   * @param in
   *          the java.io.Inputstream to read input from.
   */
  public GreekTranscribe(java.io.InputStream in) {
    this(new java.io.InputStreamReader(in));
  }


}

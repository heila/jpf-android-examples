package models;

import android.content.ContentProvider;
import android.content.ContentValues;
import android.database.Cursor;
import android.database.CursorRingdroid;
import android.net.Uri;
import android.os.CancellationSignal;

public class DefaultContentProvider extends ContentProvider {

  @Override
  public boolean onCreate() {
    return true;
  }

  @Override
  public Cursor query(Uri param0, String[] param1, String param2, String[] param3, String param4,
                      CancellationSignal param5) {
    CursorRingdroid c = new CursorRingdroid();
    c.setUri(param0);
    return c;
  }

  @Override
  public Cursor query(Uri uri, String[] projection, String selection, String[] selectionArgs, String sortOrder) {
    CursorRingdroid c = new CursorRingdroid();
    c.setUri(uri);
    return c;
  }

  @Override
  public String getType(Uri uri) {
    return uri.getAuthority();
  }

  @Override
  public Uri insert(Uri uri, ContentValues values) {
    return Uri.withAppendedPath(uri, "1");

  }

  @Override
  public int delete(Uri uri, String selection, String[] selectionArgs) {
    return 0;
  }

  @Override
  public int update(Uri uri, ContentValues values, String selection, String[] selectionArgs) {
    return 0;

  }

}

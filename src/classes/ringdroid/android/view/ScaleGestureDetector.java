package android.view;

import gov.nasa.jpf.vm.Abstraction;

public class ScaleGestureDetector {
  public class OnScaleGestureListener {

  }

  public static android.view.ScaleGestureDetector TOP = new android.view.ScaleGestureDetector();


  public ScaleGestureDetector(){
  }

  public ScaleGestureDetector(android.content.Context param0, android.view.ScaleGestureDetector.OnScaleGestureListener param1){
  }

  public boolean onTouchEvent(android.view.MotionEvent param0){
    return Abstraction.TOP_BOOL;
  }
}
package android.text.method;

public class LinkMovementMethod {
  public static android.text.method.LinkMovementMethod TOP = new android.text.method.LinkMovementMethod();


  public LinkMovementMethod(){
  }

  public static android.text.method.MovementMethod getInstance(){
    return android.text.method.MovementMethodImpl.TOP;
  }
}
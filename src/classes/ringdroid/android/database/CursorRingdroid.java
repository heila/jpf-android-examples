package android.database;

import gov.nasa.jpf.vm.AndroidVerify;

public class CursorRingdroid extends DefaultCursor {
  public int getColumnIndexOrThrow(java.lang.String param0) {
    int var = 0;
    if (param0.equals("title")) {
      var = 1;
    }
    if (param0.equals("artist")) {
      var = 2;
    }
    if (param0.equals("album")) {
      var = 3;
    }
    if (param0.equals("year")) {
      var = 4;
    }
    if (param0.equals("_data")) {
      var = 5;
    }
    return var;
  }

  public java.lang.String getString(int param0) {
    String var = "";
    if (param0 == 0) {
      var = "1";
    }
    if (param0 == 1) {
      var = AndroidVerify.getString(new String[] { "Angels" }, "android.database.Cursor.getString(1)");
    }
    if (param0 == 2) {
      var = AndroidVerify.getString(new String[] { "The XX" }, "android.database.Cursor.getString(2)");
    }
    if (param0 == 3) {
      var = AndroidVerify.getString(new String[] { "Coexist" }, "android.database.Cursor.getString(3)");
    }
    if (param0 == 5) {
      var = "/storage/sdcard/media/audio/ringtones/bensound-cute-cut.mp3";
    }
    return var;
  }

  public int getInt(int param0) {
    int var = 0;
    if (param0 == 4) {
      var = (int) AndroidVerify.getValues(new Object[] { 2012 }, "android.database.Cursor.getInt(4)");
    }
    return var;
  }

}

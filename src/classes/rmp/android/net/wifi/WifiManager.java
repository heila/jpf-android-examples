package android.net.wifi;

import android.content.Context;

public class WifiManager {

  public WifiManager(Context mSystemContext) {
  }

  public WifiLock createWifiLock(int lockType, String tag) {
    return new WifiLock();
  }

  public static class WifiLock {
    boolean acquired = false;

    private WifiLock() {
    }

    private WifiLock(int lockType, String tag) {
    }

    public void acquire() {
      acquired = true;
    }

    public void release() {
      acquired = false;
    }

    public boolean isHeld() {
      return acquired;
    }
  }
}
package android.widget;

import gov.nasa.jpf.annotation.FilterField;
import gov.nasa.jpf.util.event.events.Event;
import gov.nasa.jpf.util.event.events.UIEvent;

import java.util.LinkedList;
import java.util.List;

import android.content.Context;
import android.view.ContextMenuImpl;
import android.view.MenuImpl;

public abstract class AbsListView extends AdapterView<ListAdapter> {

  public static final int TRANSCRIPT_MODE_DISABLED = 0;
  /**
   * The list will automatically scroll to the bottom when a data set change notification is received and only
   * if the last item is already visible on screen.
   *
   * @see #setTranscriptMode(int)
   */
  public static final int TRANSCRIPT_MODE_NORMAL = 1;
  /**
   * The list will automatically scroll to the bottom, no matter what items are currently visible.
   *
   * @see #setTranscriptMode(int)
   */
  public static final int TRANSCRIPT_MODE_ALWAYS_SCROLL = 2;

  /**
   * Indicates that we are not in the middle of a touch gesture
   */
  static final int TOUCH_MODE_REST = -1;

  /**
   * Indicates we just received the touch event and we are waiting to see if the it is a tap or a scroll
   * gesture.
   */
  static final int TOUCH_MODE_DOWN = 0;

  /**
   * Indicates the touch has been recognized as a tap and we are now waiting to see if the touch is a
   * longpress
   */
  static final int TOUCH_MODE_TAP = 1;

  /**
   * Indicates we have waited for everything we can wait for, but the user's finger is still down
   */
  static final int TOUCH_MODE_DONE_WAITING = 2;

  /**
   * Indicates the touch gesture is a scroll
   */
  static final int TOUCH_MODE_SCROLL = 3;

  /**
   * Indicates the view is in the process of being flung
   */
  static final int TOUCH_MODE_FLING = 4;

  /**
   * Indicates the touch gesture is an overscroll - a scroll beyond the beginning or end.
   */
  static final int TOUCH_MODE_OVERSCROLL = 5;

  /**
   * Indicates the view is being flung outside of normal content bounds and will spring back.
   */
  static final int TOUCH_MODE_OVERFLING = 6;

  /**
   * Regular layout - usually an unsolicited layout from the view system
   */
  static final int LAYOUT_NORMAL = 0;

  /**
   * Show the first item
   */
  static final int LAYOUT_FORCE_TOP = 1;

  /**
   * Force the selected item to be on somewhere on the screen
   */
  static final int LAYOUT_SET_SELECTION = 2;

  /**
   * Show the last item
   */
  static final int LAYOUT_FORCE_BOTTOM = 3;

  /**
   * Make a mSelectedItem appear in a specific location and build the rest of the views from there. The top is
   * specified by mSpecificTop.
   */
  static final int LAYOUT_SPECIFIC = 4;

  /**
   * Layout to sync as a result of a data change. Restore mSyncPosition to have its top at mSpecificTop
   */
  static final int LAYOUT_SYNC = 5;

  /**
   * Layout as a result of using the navigation keys
   */
  static final int LAYOUT_MOVE_SELECTION = 6;

  /**
   * Normal list that does not indicate choices
   */
  public static final int CHOICE_MODE_NONE = 0;

  /**
   * The list allows up to one choice
   */
  public static final int CHOICE_MODE_SINGLE = 1;

  /**
   * The list allows multiple choices
   */
  public static final int CHOICE_MODE_MULTIPLE = 2;

  /**
   * The list allows multiple choices in a modal selection mode
   */
  public static final int CHOICE_MODE_MULTIPLE_MODAL = 3;
  @FilterField
  AdapterDataSetObserver mDataSetObserver;

  ListAdapter mAdapter;
  @FilterField
  EditText mTextFilter;

  public AbsListView(Context context) {
    super(context);
    initAbsListView();
  }

  private void initAbsListView() {
    // Setting focusable in touch mode will set the focusable property to true
    setClickable(true);
    setFocusableInTouchMode(true);
    setWillNotDraw(false);
    // setAlwaysDrawnWithCacheEnabled(false);
    // setScrollingCacheEnabled(true);
  }

  @Override
  public void setAdapter(ListAdapter adapter) {
    mAdapter = adapter;
  }

  @Override
  public ListAdapter getAdapter() {
    return mAdapter;

  }

  public int getCheckedItemCount() {
    return 0;
  }

  @Override
  public void setScrollBarStyle(int param0) {
  }

  public void setTextFilterEnabled(boolean param0) {
  }

  public void invalidateViews() {
  }

  public void setSmoothScrollbarEnabled(boolean enabled) {
  }

  public void setScrollingCacheEnabled(boolean enabled) {
  }

  public void setFastScrollEnabled(final boolean enabled) {
  }

  @Override
  public List<Event> collectEvents() {
    List<Event> events = new LinkedList<Event>();

    System.out.println("CHECK3 " + (getAdapter() != null) + " " + (mOnItemClickListener != null) + " "
        + ((getAdapter() != null) ? getAdapter().getCount() : ""));
    if (getAdapter() != null) {
      if (mOnItemClickListener != null && getAdapter().getCount() > 0) {
        for (int i = 0; i < getAdapter().getCount(); i++) {
          // String s = (String) getAdapter().getItem(i);
          UIEvent uiEvent = new UIEvent("$" + this.getName(), "onClickItem", new Object[] { i });
          events.add(uiEvent);
        }
      }
    }
    events.addAll(super.collectEvents());
    return events;
  }

  @Override
  public boolean processEvent(Event event) {
    System.out.println("CHECK4 " + (getAdapter() != null) + " " + (mOnItemClickListener != null) + " "
        + ((getAdapter() != null) ? getAdapter().getCount() : "") + (event instanceof UIEvent)
        + ((UIEvent) event).getAction().equals("onClickItem") + "event " + event);

    if (mOnItemClickListener != null && event instanceof UIEvent
        && ((UIEvent) event).getAction().equals("onClickItem")) {
      if (getAdapter() != null) {
        System.out.println("CHECK4.1 " + event);
        int pos = (int) ((UIEvent) event).getArguments()[0];
        (this).mOnItemClickListener.onItemClick(this, (this).getChildAt(pos), pos, pos);
        return true;
      }
    }
    return super.processEvent(event);
  }

  @Override
  public void performCreateContextMenu() {
    if ((this).getChildAt(0) != null) {
      menu = new ContextMenuImpl(getContext());
      AdapterContextMenuInfo menuinfo = new AdapterContextMenuInfo((this).getChildAt(0), 0, 0);
      ((MenuImpl) menu).setInfo(menuinfo);

      ListenerInfo li = mListenerInfo;
      if (li != null && li.mOnCreateContextMenuListener != null) {
        li.mOnCreateContextMenuListener.onCreateContextMenu(menu, (this).getChildAt(0), menuinfo);
      }
    }
  }

}

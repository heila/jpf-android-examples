package android.database;

import gov.nasa.jpf.vm.Abstraction;


public class CursorAnyCut extends DefaultCursor {

  public long getLong(int param0) {
    return Abstraction.TOP_LONG;
  }

  public java.lang.String getString(int param0) {
    return Abstraction.TOP_STRING;
  }

  public int getInt(int param0) {
    return Abstraction.TOP_INT;
  }

}
package android.view;
import gov.nasa.jpf.vm.Abstraction;

public final class MotionEvent {
  public static final android.view.MotionEvent TOP = new android.view.MotionEvent();


  public MotionEvent(){
  }

  public final int getAction(){
    return Abstraction.TOP_INT;
  }
}
package android.widget;

import gov.nasa.jpf.annotation.FilterField;
import android.content.Context;

public class AbsSeekBar extends android.widget.ProgressBar {

  @FilterField
  int max = 100;

  public AbsSeekBar(Context context) {
    super(context);
  }

  public synchronized void setMax(int param0) {
    max = param0;
  }
}
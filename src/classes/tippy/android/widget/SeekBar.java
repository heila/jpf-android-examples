package android.widget;

import gov.nasa.jpf.util.event.events.Event;
import gov.nasa.jpf.util.event.events.UIEvent;

import java.util.LinkedList;
import java.util.List;

import android.content.Context;

public class SeekBar extends android.widget.AbsSeekBar {

  public SeekBar(Context context) {
    super(context);
  }

  public interface OnSeekBarChangeListener {

    /**
     * Notification that the progress level has changed. Clients can use the fromUser parameter to distinguish
     * user-initiated changes from those that occurred programmatically.
     * 
     * @param seekBar
     *          The SeekBar whose progress has changed
     * @param progress
     *          The current progress level. This will be in the range 0..max where max was set by
     *          {@link ProgressBar#setMax(int)}. (The default value for max is 100.)
     * @param fromUser
     *          True if the progress change was initiated by the user.
     */
    void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser);

    /**
     * Notification that the user has started a touch gesture. Clients may want to use this to disable
     * advancing the seekbar.
     * 
     * @param seekBar
     *          The SeekBar in which the touch gesture began
     */
    void onStartTrackingTouch(SeekBar seekBar);

    /**
     * Notification that the user has finished a touch gesture. Clients may want to use this to re-enable
     * advancing the seekbar.
     * 
     * @param seekBar
     *          The SeekBar in which the touch gesture began
     */
    void onStopTrackingTouch(SeekBar seekBar);
  }

  private OnSeekBarChangeListener mOnSeekBarChangeListener;

  /**
   * Sets a listener to receive notifications of changes to the SeekBar's progress level. Also provides
   * notifications of when the user starts and stops a touch gesture within the SeekBar.
   * 
   * @param l
   *          The seek bar notification listener
   * 
   * @see SeekBar.OnSeekBarChangeListener
   */
  public void setOnSeekBarChangeListener(OnSeekBarChangeListener l) {
    mOnSeekBarChangeListener = l;
  }

  @Override
  public List<Event> collectEvents() {
    List<Event> events = new LinkedList<Event>();

    if (mOnSeekBarChangeListener != null) {
      // does not have unique id so we use class name as name;
      String name = this.getName();
      if (name == null) {
        this.setName(this.getClass().getSimpleName());
        name = this.getName();
      }
      UIEvent uiEvent = new UIEvent("$" + name, "onSeek");
      events.add(uiEvent);
    }
    events.addAll(super.collectEvents());
    return events;
  }

  @Override
  public boolean processEvent(Event event) {
    if (mOnSeekBarChangeListener != null && event instanceof UIEvent
        && ((UIEvent) event).getAction().equals("onSeek")) {

      mOnSeekBarChangeListener.onStartTrackingTouch(this);
      mOnSeekBarChangeListener.onProgressChanged(this, 10, true);
      mOnSeekBarChangeListener.onStopTrackingTouch(this);
      return true;
    }

    return super.processEvent(event);
  }

}
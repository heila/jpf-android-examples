package android.text.method;

import android.text.InputType;
import android.text.Spanned;

public class NumberKeyListener {

  public int getInputType() {
    return InputType.TYPE_NULL;
  }

  public CharSequence filter(CharSequence source, int start, int end, Spanned dest, int dstart, int dend) {
    return dest;
  }

}

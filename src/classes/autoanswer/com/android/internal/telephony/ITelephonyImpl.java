package com.android.internal.telephony;

import gov.nasa.jpf.vm.AndroidVerify;
import android.os.RemoteException;

public class ITelephonyImpl implements com.android.internal.telephony.ITelephony {
  public static com.android.internal.telephony.ITelephonyImpl TOP = new com.android.internal.telephony.ITelephonyImpl();

  @Override
  public void silenceRinger() {
  }

  boolean answered = false;

  @Override
  public void answerRingingCall() throws RemoteException {
    boolean b = (boolean) AndroidVerify.getValues(new Boolean[] { true, false },
        "ITelephonyImpl.answerRingingCall");
    if (b) {
      throw new RemoteException("Could not answer call");
    } else
      answered = true;
  }
}
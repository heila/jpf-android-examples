package com.android.internal.telephony;

public interface ITelephony {

  public void silenceRinger() throws android.os.RemoteException;

  public void answerRingingCall() throws android.os.RemoteException;
}
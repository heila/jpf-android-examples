package android.media;

import gov.nasa.jpf.vm.AndroidVerify;

public class AudioManager {
  public static final int MODE_IN_CALL = 2;
  public static final int STREAM_MUSIC = 0;

  public AudioManager() {
  }

  public int getMode() {
    int mode = (int) AndroidVerify.getValues(new Integer[] { 1, 2 }, "AudioManager.getMode()");
    return mode;
  }

  boolean state = false;
  public void setSpeakerphoneOn(boolean param0) {
    state = param0;
  }
}
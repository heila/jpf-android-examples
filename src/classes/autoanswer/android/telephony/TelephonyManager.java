package android.telephony;

import gov.nasa.jpf.vm.AndroidVerify;
import android.content.Context;
import android.os.ServiceManager;

import com.android.internal.telephony.ITelephony;
import com.android.internal.telephony.ITelephonyImpl;

public class TelephonyManager {
  public static final java.lang.String EXTRA_STATE_RINGING = "RINGING";

  public TelephonyManager(Context context) {
  }

  private TelephonyManager() {
  }

  public static TelephonyManager getDefault() {
    return (TelephonyManager) ServiceManager.getSystemService(Context.TELEPHONY_SERVICE);
  }

  public int getCallState() {
    int callState = (int) AndroidVerify.getValues(new Integer[] { 0, 1 }, "TelephonyManager.getCallState()");
    return callState;
  }

  public ITelephony getITelephony() {
    return ITelephonyImpl.TOP;
  }
}
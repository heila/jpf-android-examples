package android.bluetooth;

import gov.nasa.jpf.vm.AndroidVerify;
import android.app.Service;
import android.content.Intent;
import android.os.IBinder;

public class IBluetoothHeadsetImpl extends Service implements android.bluetooth.IBluetoothHeadset {

  @Override
  public int getState() {
    return (int) AndroidVerify.getValues(new Integer[] { -1, 2 }, "IBluetoothHeadsetImpl.getState()");
  }

  @Override
  public IBinder onBind(Intent intent) {
    return this;
  }
}
import java.io.IOException;


public class RMP {
  public static void main(String[] args) throws IOException {
    int SEARCH_DEPTH = 1000;
    int EVENT_DEPTH = 20 ;
    String EVENT_STRATEGY = "heuristic";

    String[] config_properties = { "-show", "src/examples/TestRMP.jpf",
        "+search.depth_limit=" + SEARCH_DEPTH, "+event.max_depth=" + EVENT_DEPTH,
        "+event.strategy=" + EVENT_STRATEGY };

    RunJPFAndroid run = new RunJPFAndroid("rmp", config_properties, args);
    run.runJPF();
  }
}

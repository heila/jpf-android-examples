import java.io.IOException;

public class K9 {

  public static void main(String[] args) throws IOException {
    int SEARCH_DEPTH = 1000;
    int EVENT_DEPTH = 20;
    String EVENT_STRATEGY = "heuristic";

    String[] config_properties = { "-show", "src/examples/TestK9.jpf", "+search.depth_limit=" + SEARCH_DEPTH,
        "+event.max_depth=" + EVENT_DEPTH, "+event.strategy=" + EVENT_STRATEGY,
        "+jpf-android.excludeFromPreloading=" + "com.fsck.k9.mail.store.TrustManagerFactory" };

    RunJPFAndroid run = new RunJPFAndroid("k9", config_properties, args);

    run.runJPF();
  }

}

import java.io.File;
import java.io.IOException;

public class Ringdroid {

  public static void main(String[] args) throws IOException {
    int SEARCH_DEPTH = 1000;
    int EVENT_DEPTH = 20;
    String EVENT_STRATEGY = "heuristic";

    String[] config_properties = { "-show", "src/examples/TestRingdroid.jpf",
        "+search.depth_limit=" + SEARCH_DEPTH, "+event.max_depth=" + EVENT_DEPTH,
        "+event.strategy=" + EVENT_STRATEGY };

    RunJPFAndroid run = new RunJPFAndroid("ringdroid", config_properties, args);
    run.copyFile("/storage/bensound-cute-cut.mp3", new File("/storage/sdcard/media/audio/ringtones"));
    run.copyFile("/storage/bensound-cute-cut.mp3", new File("/storage/sdcard/"));

    run.deleteFile( "/storage/sdcard/silence" + ".mp3");
    run.runJPF();
  }

}
